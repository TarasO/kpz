﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Oliinyk.Taras.RobotChallenge
{
    public class DistanceHelper
    {
        public static int FindDistance(Position a, Position b)
        {
            return (int)Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
        public static Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;

            foreach (var station in map.Stations)
            {
                if (RobotsAroundStation(station.Position, movingRobot, robots) < 2)
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);

                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }

            return nearest == null ? null : nearest.Position;
        }

        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {

            return IsCellFree(station.Position, movingRobot, robots);
        }
        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

        public static int SphericalComputation(int a, int stationAxisPosition)
        {
            if (a < 0)
            {
                return 100 - Math.Abs(stationAxisPosition + Math.Abs(a));
            }
            else if (a > 99)
            {
                return Math.Abs(stationAxisPosition - a) - 1;
            }

            return a;
        }

        public static Position FindBestPosition(Position stationPosition, Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            if (stationPosition == null)
            {
                return null;
            }

            int maxStations = 1;
            Position bestPosition = new Position(stationPosition.X, stationPosition.Y);
            for (int i = stationPosition.X-2; i <= stationPosition.X+2; i++)
            {
                int initI = i;
                if (i < 0 || i > 99)
                {
                    i = SphericalComputation(i, stationPosition.X);
                }

                for (int j = stationPosition.Y-2; j <= stationPosition.Y+2; j++)
                {
                    int initJ = j;
                    if (j < 0 || j > 99)
                    {
                        j = SphericalComputation(j, stationPosition.Y);
                    }

                    Position currentCell = new Position(i, j);
                    if ((i == stationPosition.X && j == stationPosition.Y) ||
                        IsCellAStation(currentCell, map) || IsCellOccupiedByMyRobot(currentCell, robots))
                    {
                        continue;
                    }

                    int stationCount = StationNumber(currentCell, map);
                    if (stationCount > maxStations)
                    {
                        maxStations = stationCount;
                        bestPosition = currentCell;
                    }
                    else if(stationCount == maxStations)
                    {
                        if (FindDistance(currentCell, movingRobot.Position) <
                            FindDistance(bestPosition, movingRobot.Position))
                        {
                            bestPosition = currentCell;
                        }
                    }

                    j = initJ;
                }

                i = initI;
            }

            return bestPosition;
        }

        public static bool IsCellAStation(Position cell, Map map)
        {

            return map.Stations.Any(s => s.Position == cell);
        }

        public static int StationNumber(Position cell, Map map)
        {
            int stationCount = 0;


            for (int i = cell.X-2; i <= cell.X+2; i++)
            {
                int initI = i;
                if (i < 0 || i > 99)
                {
                    i = SphericalComputation(i, cell.X);
                }

                for (int j = cell.Y-2; j <= cell.Y+2; j++)
                {

                    int initJ = j;
                    if (j < 0 || j > 99)
                    {
                        j = SphericalComputation(j, cell.Y);
                    }

                    Position currentCell = new Position(i, j);
                    if (i == cell.X && j == cell.Y) {}

                    if (IsCellAStation(currentCell, map))
                    {
                        stationCount++;
                    }

                    j = initJ;
                }

                i = initI;

            }

            return stationCount;
        }

        public static bool IsCellOccupiedByMyRobot(Position cell, IList<Robot.Common.Robot> robots)
        {
            return robots
                .Where(r => r.Position == cell).Any(r => r.OwnerName.Equals("Oliinyk Taras"));
        }

        public static int RobotsAroundStation(Position stationPosition, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            if (stationPosition == null)
            {
                return 2;
            }

            int robotCount = 0;
            foreach (var robot in robots)
            {
                if (robot == movingRobot)
                {
                    continue;
                }

                if (FindDistance(robot.Position, stationPosition) <= 2)
                {
                    robotCount++;
                }
            }

            return robotCount;
        }

        public static Position StationNearRobot(Robot.Common.Robot movingRobot, Map map)
        {
            for (int i = movingRobot.Position.X - 2; i <= movingRobot.Position.X + 2; i++)
            {
                for (int j = movingRobot.Position.Y - 2; j <= movingRobot.Position.Y + 2; j++)
                {

                    Position currentCell = new Position(i, j);

                    if (IsCellAStation(currentCell, map))
                    {
                        return currentCell;
                    }
                }

            }

            return null;
        }
    }
}
