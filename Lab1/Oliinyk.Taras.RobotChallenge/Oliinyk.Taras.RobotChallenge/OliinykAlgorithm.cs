using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;
using Oliinyk.Taras.RobotChallenge;


namespace Oliinyk.Taras.RobotChallenge
{
    public class OliinykAlgorithm : IRobotAlgorithm
    {
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if (movingRobot.Energy > 250 && (robots.Count < map.Stations.Count*2))
            {
                return new CreateNewRobotCommand();
            }

            Position stationPosition = DistanceHelper.FindNearestFreeStation(robots[robotToMoveIndex], map, robots);

            Position positionToMove = DistanceHelper.FindBestPosition(stationPosition, movingRobot, map, robots);

            if (positionToMove == null)
            {
                return null;
            }

            if (DistanceHelper.FindDistance(stationPosition, movingRobot.Position) <= 2)
            {
                return new CollectEnergyCommand();
            }
            else
            {
                int distance = DistanceHelper.FindDistance(movingRobot.Position, positionToMove);
                int neededEnergy = (int)Math.Pow(distance, 2);

                if (movingRobot.Energy < neededEnergy)
                {
                    int x = movingRobot.Position.X + movingRobot.Position.X.CompareTo(positionToMove.X)*-1;

                    int y = movingRobot.Position.Y + movingRobot.Position.Y.CompareTo(positionToMove.Y)*-1;

                    positionToMove = new Position(x, y);
                }

                return new MoveCommand() {NewPosition = positionToMove};
            }
        }

        public string Author
        {
            get { return "Oliinyk Taras"; }
        }
    }
}
