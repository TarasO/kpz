﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Channels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oliinyk.Taras.RobotChallenge;
using Robot.Common;

namespace Oliinyk.Taras.RobotChallenge.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CollectEnergyTest()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(1,1), Energy = 100});
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(3, 1), Energy = 200, OwnerName = "Oliinyk Taras"});

            OliinykAlgorithm algorithm = new OliinykAlgorithm();

            RobotCommand command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void CreateNewRobotTest()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 0), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(10, 0), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(15, 0), Energy = 100 });
            
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(99, 0), Energy = 300, OwnerName = "Oliinyk Taras"});
            
            OliinykAlgorithm algorithm = new OliinykAlgorithm();
            
            RobotCommand command = algorithm.DoStep(robots, 0, map);
            
            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void MoveToClosestStationTest()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 0), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(10, 0), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(15, 0), Energy = 100 });

            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(5, 0), Energy = 200, OwnerName = "Oliinyk Taras" });

            OliinykAlgorithm algorithm = new OliinykAlgorithm();

            MoveCommand command = (MoveCommand)algorithm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(4, 0), command.NewPosition);
        }

        [TestMethod]
        public void MoveToFreeStationTest()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 0), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(10, 0), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(15, 0), Energy = 100 });

            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(5, 0), Energy = 200, OwnerName = "Oliinyk Taras" });
            robots.Add(new Robot.Common.Robot() { Position = new Position(3, 0), Energy = 200, OwnerName = "Robot Robot" });
            robots.Add(new Robot.Common.Robot() { Position = new Position(2, 1), Energy = 200, OwnerName = "Robot Robot" });

            OliinykAlgorithm algorithm = new OliinykAlgorithm();

            MoveCommand command = (MoveCommand)algorithm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(8, 0), command.NewPosition);
        }

        [TestMethod]
        public void MoveBetweenStationsTest()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 0), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(4, 0), Energy = 100 });

            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 3), Energy = 200, OwnerName = "Oliinyk Taras" });

            OliinykAlgorithm algorithm = new OliinykAlgorithm();

            MoveCommand command = (MoveCommand)algorithm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(2, 2), command.NewPosition);
        }

        [TestMethod]
        public void MoveBeyondTheBoundaryTest()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(98, 0), Energy = 100 });

            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 200, OwnerName = "Oliinyk Taras" });

            OliinykAlgorithm algorithm = new OliinykAlgorithm();

            MoveCommand command = (MoveCommand)algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(new Position(1, 0), command.NewPosition);
        }

        [TestMethod]
        public void NoStationsTest()
        {
            Map map = new Map();

            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 200, OwnerName = "Oliinyk Taras" });

            OliinykAlgorithm algorithm = new OliinykAlgorithm();

            RobotCommand command = algorithm.DoStep(robots, 0, map);

            Assert.IsNull(command);
        }

        [TestMethod]
        public void AttackTest()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 0), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(4, 0), Energy = 100 });

            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 3), Energy = 200, OwnerName = "Oliinyk Taras" });
            robots.Add(new Robot.Common.Robot() { Position = new Position(2, 2), Energy = 200, OwnerName = "Robot Robot" });

            OliinykAlgorithm algorithm = new OliinykAlgorithm();

            MoveCommand command = (MoveCommand)algorithm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(2, 2), command.NewPosition);
        }

        [TestMethod]
        public void DontAttackFriendlyTest()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(2, 0), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(4, 0), Energy = 100 });

            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 3), Energy = 200, OwnerName = "Oliinyk Taras" });
            robots.Add(new Robot.Common.Robot() { Position = new Position(2, 2), Energy = 200, OwnerName = "Oliinyk Taras" });
            robots.Add(new Robot.Common.Robot() { Position = new Position(5, 2), Energy = 200, OwnerName = "Robot Robot" });

            OliinykAlgorithm algorithm = new OliinykAlgorithm();

            MoveCommand command = (MoveCommand)algorithm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(2, 1), command.NewPosition);
        }

        [TestMethod]
        public void CrawlSlowly()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 0), Energy = 100 });

            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(12, 0), Energy = 30, OwnerName = "Oliinyk Taras" });

            OliinykAlgorithm algorithm = new OliinykAlgorithm();

            MoveCommand command = (MoveCommand)algorithm.DoStep(robots, 0, map);
            Assert.AreEqual(new Position(13, 0), command.NewPosition);
        }
    }
}
