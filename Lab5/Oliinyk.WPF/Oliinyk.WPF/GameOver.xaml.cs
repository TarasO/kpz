﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oliinyk.WPF
{
    /// <summary>
    /// Interaction logic for GameOver.xaml
    /// </summary>
    public partial class GameOver : Page
    {
        public ICommand AddCommand { get; set; }
        public String name { get; set; }

        public Difficulty difficulty { get; set; }

        public int score { get; set; }

        public GameOver(Difficulty difficulty, int score)
        {
            AddCommand = ((ApplicationViewModel)App.ParentWindowRef.DataContext).AddCommand;
            this.difficulty = difficulty;
            this.score = score;

            InitializeComponent();
            this.DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.ParentWindowRef.ParentFrame.Navigate(new MainMenu());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Record record = new Record(name, difficulty, score);
            if (AddCommand.CanExecute(record))
            {
                AddCommand.Execute(record);
            }
        }
    }
}
