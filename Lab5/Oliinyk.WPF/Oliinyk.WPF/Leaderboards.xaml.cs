﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oliinyk.WPF
{
    /// <summary>
    /// Interaction logic for Leaderboards.xaml
    /// </summary>
    public partial class Leaderboards : Page
    {
        public ObservableCollection<Record> easy { get; }
        public ObservableCollection<Record> medium { get; }
        public ObservableCollection<Record> hard { get; }

        public Leaderboards()
        {
            ApplicationViewModel appVM = (ApplicationViewModel)App.ParentWindowRef.DataContext;
            easy = appVM.getByDifficulty(Difficulty.Easy);
            medium = appVM.getByDifficulty(Difficulty.Medium);
            hard = appVM.getByDifficulty(Difficulty.Hard);
            
            InitializeComponent();
            this.DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.ParentWindowRef.ParentFrame.Navigate(new MainMenu());
        }
    }
}
