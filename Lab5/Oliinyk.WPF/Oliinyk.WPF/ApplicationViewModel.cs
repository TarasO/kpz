﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.WPF
{
    class ApplicationViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Record> records { get; set; }

        public ApplicationViewModel()
        {
            records = new ObservableCollection<Record>();
            records.Add(new Record("LolE", Difficulty.Easy, 1000));
            records.Add(new Record("KekE", Difficulty.Easy, 2000));
            records.Add(new Record("LolM", Difficulty.Medium, 3000));
            records.Add(new Record("KekM", Difficulty.Medium, 2500));
            records.Add(new Record("LolH", Difficulty.Hard, 5000));
            records.Add(new Record("KekH", Difficulty.Hard, 4500));
        }

        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ??
                       (addCommand = new RelayCommand(o =>
                       {
                           records.Add((Record)o);
                       }, o =>
                       {
                           Record record = (Record) o;
                           if (record.name == null || record.name.Equals(""))
                           {
                               return false;
                           }

                           return true;
                       }));
            }
        }

        public ObservableCollection<Record> getByDifficulty(Difficulty difficulty)
        {
            return new ObservableCollection<Record>(records.Where(x => x.difficulty == difficulty).ToList());
        }

        public Record getTopRecordByDifficulty(Difficulty difficulty)
        {
            return records.Where(x => x.difficulty == difficulty).OrderByDescending(x => x.score).First();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
