﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oliinyk.WPF
{
    /// <summary>
    /// Interaction logic for Battle.xaml
    /// </summary>
    public partial class Battle : Page
    {
        private Difficulty difficulty { get; }
        public Battle(Difficulty difficulty)
        {
            this.difficulty = difficulty;
            InitializeComponent();
            ApplicationViewModel appVM = (ApplicationViewModel)App.ParentWindowRef.DataContext;
            TopRecord.record = appVM.getTopRecordByDifficulty(difficulty);
            this.DataContext = this;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            int score = random.Next(2000);
            if (difficulty == Difficulty.Medium)
            {
                score = (int)(score * 4);
            } else if (difficulty == Difficulty.Hard)
            {
                score = (int) (score * 7);
            }

            App.ParentWindowRef.ParentFrame.Navigate(new GameOver(difficulty, score));
        }
    }
}
