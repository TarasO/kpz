﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.WPF
{
    public enum Difficulty
    {
        Easy, Medium, Hard
    }

    public class Record
    {
        public Record(String name, Difficulty difficulty, int score)
        {
            this.name = name;
            this.difficulty = difficulty;
            this.score = score;
        }

        public String name { get; }
        public Difficulty difficulty { get; }
        public int score { get; }
    }
}
