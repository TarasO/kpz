﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.Project
{
    public class Player
    {
        private string name {get;}
        public Player(string name)
        {
            this.name = name;
        }

        public void Shoot(Enemy enemy)
        {
            enemy.Health -= 50;
        }
    }
}
