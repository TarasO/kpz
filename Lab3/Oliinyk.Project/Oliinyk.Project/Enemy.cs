﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.Project
{
    public enum EnemyType
    {
        REDHAWK, YELLOWOWL, GREENBAT
    }

    public class Enemy
    {
        public Enemy(int health, EnemyType type)
        {
            this.Health = health;
            this.Type = type;
        }

        public int Health { get; set; }

        public EnemyType Type { get; set; }

    }

    public class EnemyGenericComparator : IComparer<Enemy>
    {
        public int Compare(Enemy x, Enemy y)
        {
            return x.Health.CompareTo(y.Health);
        }
    }
    public class EnemyComparator : IComparer
    {
        public int Compare(object x, object y)
        {
            Enemy xEnemy = (Enemy)x;
            Enemy yEnemy = (Enemy)y;
            return xEnemy.Health.CompareTo(yEnemy.Health);
        }
    }
}
