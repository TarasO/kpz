﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Oliinyk.Project;

namespace Oliinyk.ProjectTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestKill()
        {
            List<Enemy> enemies = new List<Enemy>();
            enemies.Add(new Enemy(50, EnemyType.GREENBAT));
            enemies.Add(new Enemy(80, EnemyType.GREENBAT));
            enemies.Add(new Enemy(50, EnemyType.GREENBAT));
            enemies.Add(new Enemy(80, EnemyType.GREENBAT));
            enemies.Add(new Enemy(50, EnemyType.GREENBAT));
            Oliinyk.Project.Player player = new Player("Vjux");
            enemies.Where(x => x.Health >= 80).ToList().ForEach(x => player.Shoot(x));
            Assert.AreEqual(enemies.Where(x => x.Health >= 80).Count(), 0);
        }

        [TestMethod]
        public void TestCompareSortByHP()
        {
            List<Enemy> actual =
                new List<Enemy>
                {
                    new Enemy(30, EnemyType.GREENBAT),
                    new Enemy(20, EnemyType.GREENBAT),
                    new Enemy(10, EnemyType.GREENBAT),
                    new Enemy(50, EnemyType.GREENBAT),
                    new Enemy(40, EnemyType.GREENBAT)
                };
            List<Enemy> expected =
                new List<Enemy>
                {
                    new Enemy(10, EnemyType.GREENBAT),
                    new Enemy(20, EnemyType.GREENBAT),
                    new Enemy(30, EnemyType.GREENBAT),
                    new Enemy(40, EnemyType.GREENBAT),
                    new Enemy(50, EnemyType.GREENBAT)
                };
            EnemyComparator enemyComparator = new EnemyComparator();
            EnemyGenericComparator enemyGenericComparator = new EnemyGenericComparator();
            actual.Sort(enemyGenericComparator);
            actual.ForEach(x => Console.WriteLine(x.Health));
            expected.ForEach(x => Console.WriteLine(x.Health));
            CollectionAssert.AreEqual(actual, expected, enemyComparator);
        }

        [TestMethod]
        public void TestGroupByTypeCount()
        {
            List<Enemy> enemies =
                new List<Enemy>
                {
                    new Enemy(30, EnemyType.GREENBAT),
                    new Enemy(20, EnemyType.GREENBAT),
                    new Enemy(10, EnemyType.YELLOWOWL),
                    new Enemy(50, EnemyType.REDHAWK),
                    new Enemy(40, EnemyType.REDHAWK)
                };

            var enemyGroup = from enemy in enemies
                group enemy by enemy.Type
                into g
                select new {Type = g.Key, Count = g.Count()};

            Assert.AreEqual(enemyGroup.Where(e => e.Type == EnemyType.GREENBAT).First().Count, 2);
            Assert.AreEqual(enemyGroup.Where(e => e.Type == EnemyType.YELLOWOWL).First().Count, 1);
            Assert.AreEqual(enemyGroup.Where(e => e.Type == EnemyType.REDHAWK).First().Count, 2);
        }
    }
}
