﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFirstNoEF
{
    class Production
    {
        public int id { get; set; }
        public string sweet { get; set; }
        public decimal amount { get; set; }
        public string shop { get; set; }
        public DateTime date { get; set; }

        public Production() { }

        public Production(int id, string sweet, decimal amount, string shop, DateTime date)
        {
            this.id = id;
            this.sweet = sweet;
            this.amount = amount;
            this.shop = shop;
            this.date = date;
        }
    }
}
