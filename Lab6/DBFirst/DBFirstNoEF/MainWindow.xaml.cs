﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DBFirstNoEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ApplicationViewModel();
        }

        //RowEditEnding
        private void Category_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            bool existance = ((ApplicationViewModel)DataContext).CheckExistance(((Category)e.Row.Item).id, "Category");
            if (existance)
            {
                ((ApplicationViewModel)DataContext).UpdateItem(e.Row.Item, "Category");
            } else
            {
                ((ApplicationViewModel)DataContext).InsertItem(e.Row.Item, "Category");
            }
            
        }
        private void Sweet_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            bool existance = ((ApplicationViewModel)DataContext).CheckExistance(((Sweet)e.Row.Item).id, "Sweet");
            if (existance)
            {
                ((ApplicationViewModel)DataContext).UpdateItem(e.Row.Item, "Sweet");
            } else
            {
                ((ApplicationViewModel)DataContext).InsertItem(e.Row.Item, "Sweet");
            }
        }
        private void Shop_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            bool existance = ((ApplicationViewModel)DataContext).CheckExistance(((Shop)e.Row.Item).id, "ConfectionaryShop");
            if (existance)
            {
                ((ApplicationViewModel)DataContext).UpdateItem(e.Row.Item, "ConfectionaryShop");
            } else
            {
                ((ApplicationViewModel)DataContext).InsertItem(e.Row.Item, "ConfectionaryShop");
            }
        }
        private void Production_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            bool existance = ((ApplicationViewModel)DataContext).CheckExistance(((Production)e.Row.Item).id, "Production");
            if (existance)
            {
                ((ApplicationViewModel)DataContext).UpdateItem(e.Row.Item, "Production");
            } else
            {
                ((ApplicationViewModel)DataContext).InsertItem(e.Row.Item, "Production");
            }
        }

        //AddingNewItem
        private void Category_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            e.NewItem = ((ApplicationViewModel)DataContext).GetNewItem("Category");
        }

        private void Sweet_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            e.NewItem = ((ApplicationViewModel)DataContext).GetNewItem("Sweet");
        }

        private void Shop_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            e.NewItem = ((ApplicationViewModel)DataContext).GetNewItem("ConfectionaryShop");
        }

        private void Production_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            e.NewItem = ((ApplicationViewModel)DataContext).GetNewItem("Production");
        }

        //PreviewKeyDown
        private void Category_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            if (dg != null)
            {
                DataGridRow dgr = (DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(dg.SelectedIndex));
                if (e.Key == Key.Delete && !dgr.IsEditing)
                {
                    ((ApplicationViewModel)DataContext).RemoveItem(((Category)dgr.Item).id, "Category");
                }
            }
        }

        private void Sweet_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            if (dg != null)
            {
                DataGridRow dgr = (DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(dg.SelectedIndex));
                if (e.Key == Key.Delete && !dgr.IsEditing)
                {
                    ((ApplicationViewModel)DataContext).RemoveItem(((Sweet)dgr.Item).id, "Sweet");
                }
            }
        }

        private void Shop_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            if (dg != null)
            {
                DataGridRow dgr = (DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(dg.SelectedIndex));
                if (e.Key == Key.Delete && !dgr.IsEditing)
                {
                    ((ApplicationViewModel)DataContext).RemoveItem(((Shop)dgr.Item).id, "ConfectionaryShop");
                }
            }
        }

        private void Production_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            if (dg != null)
            {
                DataGridRow dgr = (DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(dg.SelectedIndex));
                if (e.Key == Key.Delete && !dgr.IsEditing)
                {
                    ((ApplicationViewModel)DataContext).RemoveItem(((Production)dgr.Item).id, "Production");
                }
            }
        }

    }
}
