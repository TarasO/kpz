﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBFirstNoEF
{
    class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public string unit { get; set; }

        public Category() { }

        public Category(int id, string name, string unit)
        {
            this.id = id;
            this.name = name;
            this.unit = unit;
        }
    }
}
