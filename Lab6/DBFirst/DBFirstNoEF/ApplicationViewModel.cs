﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.Common;

namespace DBFirstNoEF
{
    class ApplicationViewModel
    {
        private SqlConnection sqlConnection;
        public ObservableCollection<Category> Categories { get; set; }
        public ObservableCollection<Sweet> Sweets { get; set; }
        public ObservableCollection<Shop> Shops { get; set; }
        public ObservableCollection<Production> Productions { get; set; }

        public ApplicationViewModel()
        {
            Categories = new ObservableCollection<Category>();
            Sweets = new ObservableCollection<Sweet>();
            Shops = new ObservableCollection<Shop>();
            Productions = new ObservableCollection<Production>();

            sqlConnection =
                new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            SqlCommand getCategoriesCommand = new SqlCommand("SELECT * FROM Category", sqlConnection);
            SqlCommand getSweetsCommand =
                new SqlCommand(
                    "SELECT s.id, s.name, c.name AS c_name, s.price_per_unit AS price FROM Sweet s INNER JOIN Category c ON s.category_id = c.id",
                    sqlConnection);
            SqlCommand getShopsCommand = new SqlCommand("SELECT * FROM ConfectionaryShop", sqlConnection);
            SqlCommand getProductionCommand = new SqlCommand(
                "SELECT p.id, s.name AS s_name, p.amount, cs.name AS cs_name, p.production_date AS date FROM Production p INNER JOIN Sweet s ON p.sweet_id = s.id INNER JOIN ConfectionaryShop cs ON p.confectionary_shop_id = cs.id",
                sqlConnection);

            sqlConnection.Open();

            DbDataReader categoryReader = getCategoriesCommand.ExecuteReader();
            if (categoryReader.HasRows)
            {
                while (categoryReader.Read())
                {
                    Categories.Add(new Category((int) categoryReader["id"], (string) categoryReader["name"],
                        (string) categoryReader["unit"]));
                }
            }

            categoryReader.Close();

            DbDataReader sweetReader = getSweetsCommand.ExecuteReader();
            if (sweetReader.HasRows)
            {
                while (sweetReader.Read())
                {
                    Sweets.Add(new Sweet((int) sweetReader["id"], (string) sweetReader["name"],
                        (string) sweetReader["c_name"], (decimal) sweetReader["price"]));
                }
            }

            sweetReader.Close();

            DbDataReader shopReader = getShopsCommand.ExecuteReader();
            if (shopReader.HasRows)
            {
                while (shopReader.Read())
                {
                    Shops.Add(new Shop((int) shopReader["id"], (string) shopReader["name"]));
                }
            }

            shopReader.Close();

            DbDataReader productionReader = getProductionCommand.ExecuteReader();
            if (productionReader.HasRows)
            {
                while (productionReader.Read())
                {
                    Productions.Add(new Production((int) productionReader["id"], (string) productionReader["s_name"],
                        (decimal) productionReader["amount"], (string) productionReader["cs_name"],
                        (DateTime) productionReader["date"]));
                }
            }

            productionReader.Close();

            sqlConnection.Close();
        }

        public void RemoveItem(int id, string entityName)
        {
            SqlCommand deleteCommand = new SqlCommand("DELETE FROM " + entityName + " WHERE id = @id", sqlConnection);
            deleteCommand.Parameters.AddWithValue("@id", id);

            sqlConnection.Open();

            deleteCommand.ExecuteNonQuery();

            sqlConnection.Close();
        }

        public void UpdateItem(object item, string entityName)
        {
            SqlCommand updateCommand = null;

            sqlConnection.Open();

            switch (entityName)
            {
                case "Category":
                    Category category = (Category)item;
                    updateCommand = new SqlCommand("UPDATE Category SET name = @name, unit = @unit WHERE id = @id", sqlConnection);
                    updateCommand.Parameters.AddWithValue("@id", category.id);
                    updateCommand.Parameters.AddWithValue("@name", category.name);
                    updateCommand.Parameters.AddWithValue("@unit", category.unit);
                    break;
                case "Sweet":
                    Sweet sweet = (Sweet)item;
                    int category_id = (int)new SqlCommand("SELECT id FROM Category WHERE name = '" + sweet.category + "'", sqlConnection).ExecuteScalar();
                    updateCommand = new SqlCommand("UPDATE Sweet Set name = @name, category_id = @category_id, price_per_unit = @price WHERE id = @id", sqlConnection);
                    updateCommand.Parameters.AddWithValue("@id", sweet.id);
                    updateCommand.Parameters.AddWithValue("@name", sweet.name);
                    updateCommand.Parameters.AddWithValue("@category_id", category_id);
                    updateCommand.Parameters.AddWithValue("@price", sweet.price);
                    break;
                case "ConfectionaryShop":
                    Shop shop = (Shop)item;
                    updateCommand = new SqlCommand("UPDATE ConfectionaryShop SET name = @name WHERE id = @id", sqlConnection);
                    updateCommand.Parameters.AddWithValue("@id", shop.id);
                    updateCommand.Parameters.AddWithValue("@name", shop.name);
                    break;
                case "Production":
                    Production production = (Production)item;
                    int sweet_id = (int) new SqlCommand("SELECT id FROM Sweet WHERE name = '" + production.sweet + "'", sqlConnection).ExecuteScalar();
                    int shop_id = (int)new SqlCommand("SELECT id FROM ConfectionaryShop WHERE name = '" + production.shop + "'", sqlConnection).ExecuteScalar();
                    updateCommand = new SqlCommand("UPDATE Production SET sweet_id = @sweet, amount = @amount, confectionary_shop_id = @shop, production_date = @date WHERE id = @id", sqlConnection);
                    updateCommand.Parameters.AddWithValue("@id", production.id);
                    updateCommand.Parameters.AddWithValue("@sweet", sweet_id);
                    updateCommand.Parameters.AddWithValue("@amount", production.amount);
                    updateCommand.Parameters.AddWithValue("@shop", shop_id);
                    updateCommand.Parameters.AddWithValue("@date", production.date);
                    break;
            }

            updateCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public void InsertItem(object item, string entityName)
        {
            SqlCommand insertCommand = null;

            sqlConnection.Open();

            switch (entityName)
            {
                case "Category":
                    Category category = (Category)item;
                    insertCommand = new SqlCommand("INSERT INTO Category(name, unit) VALUES(@name, @unit)", sqlConnection);
                    insertCommand.Parameters.AddWithValue("@name", category.name);
                    insertCommand.Parameters.AddWithValue("@unit", category.unit);
                    break;
                case "Sweet":
                    Sweet sweet = (Sweet)item;
                    int category_id = (int)new SqlCommand("SELECT id FROM Category WHERE name = '" + sweet.category + "'", sqlConnection).ExecuteScalar();
                    insertCommand = new SqlCommand("INSERT INTO Sweet(name, category_id, price_per_unit) VALUES(@name, @category_id, @price)", sqlConnection);
                    insertCommand.Parameters.AddWithValue("@name", sweet.name);
                    insertCommand.Parameters.AddWithValue("@category_id", category_id);
                    insertCommand.Parameters.AddWithValue("@price", sweet.price);
                    break;
                case "ConfectionaryShop":
                    Shop shop = (Shop)item;
                    insertCommand = new SqlCommand("INSERT INTO ConfectionaryShop(name) VALUES(@name)", sqlConnection);
                    insertCommand.Parameters.AddWithValue("@name", shop.name);
                    break;
                case "Production":
                    Production production = (Production)item;
                    int sweet_id = (int)new SqlCommand("SELECT id FROM Sweet WHERE name = '" + production.sweet + "'", sqlConnection).ExecuteScalar();
                    int shop_id = (int)new SqlCommand("SELECT id FROM ConfectionaryShop WHERE name = '" + production.shop + "'", sqlConnection).ExecuteScalar();
                    insertCommand = new SqlCommand("INSERT INTO Production(sweet_id, amount, confectionary_shop_id, production_date) VALUES(@sweet, @amount, @shop, @date)", sqlConnection);
                    insertCommand.Parameters.AddWithValue("@sweet", sweet_id);
                    insertCommand.Parameters.AddWithValue("@amount", production.amount);
                    insertCommand.Parameters.AddWithValue("@shop", shop_id);
                    insertCommand.Parameters.AddWithValue("@date", production.date);
                    break;
            }

            insertCommand.ExecuteNonQuery();
            sqlConnection.Close();
        }

        public object GetNewItem(string entityName)
        {
            SqlCommand getIdCommand = new SqlCommand("SELECT (MAX(id)+1) FROM " + entityName, sqlConnection);
            sqlConnection.Open();
            int o_id = (int)getIdCommand.ExecuteScalar();
            sqlConnection.Close();

            switch (entityName)
            {
                case "Category":
                    Category category = new Category {id = o_id, name = "Placeholder", unit = "Placeholder"};
                    return category;
                case "Sweet":
                    Sweet sweet = new Sweet {id = o_id, name = "Placeholder", category = "Placeholder", price = 0};
                    return sweet;
                case "ConfectionaryShop":
                    Shop shop = new Shop {id = o_id, name = "Placeholder"};
                    return shop;
                case "Production":
                    Production production = new Production {id = o_id, sweet = "PlaceHolder", amount = 0,  shop = "Placeholder", date = DateTime.Now};
                    return production;
            }

            return null;
        }

        public bool CheckExistance(int id, string entityName)
        {
            SqlCommand existanceCommand = new SqlCommand("SELECT COUNT(*) FROM " + entityName + " WHERE id = @id", sqlConnection);
            existanceCommand.Parameters.AddWithValue("@id", id);
            sqlConnection.Open();
            int count = (int)existanceCommand.ExecuteScalar();
            sqlConnection.Close();
            if (count > 0)
            {
                return true;
            }
            return false;
        }
    }
}