﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace DBFirst
{
    class ApplicationViewModel
    {
        public SweetsEntities db;
        public ObservableCollection<Category> Categories { get; set; }
        public ObservableCollection<SweetRow> Sweets { get; set; }
        public ObservableCollection<ConfectionaryShop> Shops { get; set; }
        public ObservableCollection<ProductionRow> Productions { get; set; }

        public ApplicationViewModel()
        {
            db = new SweetsEntities();

            db.Category.Load();
            Categories = db.Category.Local;

            db.Sweet.Load();
            Sweets = new ObservableCollection<SweetRow>(db.Sweet.Local.Join(Categories, s => s.category_id, c => c.id, (s, c) => new SweetRow(s.id, s.name, c.name, s.price_per_unit)));

            db.ConfectionaryShop.Load();
            Shops = db.ConfectionaryShop.Local;

            db.Production.Load();
            Productions = new ObservableCollection<ProductionRow>(from s in db.Sweet.Local
                join p in db.Production.Local on s.id equals p.sweet_id
                join cs in db.ConfectionaryShop.Local on p.confectionary_shop_id equals cs.id
                select new ProductionRow(p.id, s.name, p.amount, cs.name, p.production_date));
        }

        public void RemoveItem(int id, string entityName)
        {
            switch (entityName)
            {
                case "Sweet":
                    Sweet sweet = db.Sweet.Find(id);
                    if(sweet != null){
                        db.Sweet.Remove(sweet);
                    }
                    break;
                case "Production":
                    Production production = db.Production.Find(id);
                    if (production != null)
                    {
                        db.Production.Remove(production);
                    }
                    break;
            }
        }

        public void UpsertItem(object item, string entityName)
        {
            switch (entityName)
            {
                case "Sweet":
                    SweetRow sweetRow = (SweetRow) item;
                    Category category = db.Category.Local.First(o => o.name.Equals(sweetRow.category));
                    Sweet sweet = new Sweet {id = sweetRow.id, category_id = category.id, name = sweetRow.name, price_per_unit = sweetRow.price};
                    db.Sweet.AddOrUpdate(sweet);
                    break;
                case "Production":
                    ProductionRow productionRow = (ProductionRow)item;
                    Sweet productedSweet = db.Sweet.Local.First(o => o.name.Equals(productionRow.sweet));
                    ConfectionaryShop shop = db.ConfectionaryShop.First(o => o.name.Equals(productionRow.shop));
                    Production production = new Production {id = productionRow.id, amount = productionRow.amount, confectionary_shop_id = shop.id,
                        production_date = productionRow.date, sweet_id = productedSweet.id};
                    db.Production.AddOrUpdate(production);
                    break;
            }
        }

        public object GetNewItem(string entityName)
        {
            switch (entityName)
            {
                case "Sweet":
                    SweetRow sweet = new SweetRow {id = db.Sweet.Max(o => o.id) + 1, name = "Placeholder", category = "Placeholder", price = 0};
                    return sweet;
                case "Production":
                    ProductionRow production = new ProductionRow {id = db.Production.Max(o => o.id) + 1, sweet = "Placeholder", amount = 0, shop = "Placeholder", date = DateTime.Now};
                    return production;
            }

            return -1;
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }
    }
}
