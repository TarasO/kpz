﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DBFirst
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new ApplicationViewModel();
        }

        //RowEditEnding
        private void Sweet_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            ((ApplicationViewModel)DataContext).UpsertItem(e.Row.Item, "Sweet");
        }

        private void Production_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            ((ApplicationViewModel)DataContext).UpsertItem(e.Row.Item, "Production");
        }

        //Click
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((ApplicationViewModel)DataContext).SaveChanges();
        }

        //AddingNewItem
        private void Sweet_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            e.NewItem = ((ApplicationViewModel)DataContext).GetNewItem("Sweet");
        }

        private void Production_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            e.NewItem = ((ApplicationViewModel)DataContext).GetNewItem("Production");
        }

        //PreviewKeyDown
        private void Sweet_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            if (dg != null)
            {
                DataGridRow dgr = (DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(dg.SelectedIndex));
                if (e.Key == Key.Delete && !dgr.IsEditing)
                {
                    ((ApplicationViewModel)DataContext).RemoveItem(((SweetRow)dgr.Item).id, "Sweet");
                }
            }
        }

        private void Production_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            if (dg != null)
            {
                DataGridRow dgr = (DataGridRow)(dg.ItemContainerGenerator.ContainerFromIndex(dg.SelectedIndex));
                if (e.Key == Key.Delete && !dgr.IsEditing)
                {
                    ((ApplicationViewModel)DataContext).RemoveItem(((ProductionRow)dgr.Item).id, "Production");
                }
            }
        }
    }
}
