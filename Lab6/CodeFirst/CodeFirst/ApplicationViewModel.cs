﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst
{
    class ApplicationViewModel
    {
        public SweetsContext db;
        public ObservableCollection<Category> Categories { get; set; }
        public ObservableCollection<SweetRow> Sweets { get; set; }
        public ObservableCollection<ConfectionaryShop> Shops { get; set; }
        public ObservableCollection<ProductionRow> Productions { get; set; }

        public ApplicationViewModel()
        {
            db = new SweetsContext();

            db.Category.Load();
            Categories = db.Category.Local;

            db.Sweet.Load();
            Sweets = new ObservableCollection<SweetRow>(db.Sweet.Local.Join(Categories, s => s.Category, c => c, (s, c) => new SweetRow(s.id, s.name, c.name, s.price_per_unit)));

            db.ConfectionaryShop.Load();
            Shops = db.ConfectionaryShop.Local;

            db.Production.Load();
            Productions = new ObservableCollection<ProductionRow>(from s in db.Sweet.Local
                                                                  join p in db.Production.Local on s equals p.Sweet
                                                                  join cs in db.ConfectionaryShop.Local on p.ConfectionaryShop equals cs
                                                                  select new ProductionRow(p.id, s.name, p.amount, cs.name, p.production_date));
        }

        public void RemoveItem(int id, string entityName)
        {
            switch (entityName)
            {
                case "Sweet":
                    Sweet sweet = db.Sweet.Find(id);
                    if (sweet != null)
                    {
                        db.Sweet.Remove(sweet);
                    }
                    break;
                case "Production":
                    Production production = db.Production.Find(id);
                    if (production != null)
                    {
                        db.Production.Remove(production);
                    }
                    break;
            }
        }

        public void UpsertItem(object item, string entityName)
        {
            switch (entityName)
            {
                case "Sweet":
                    SweetRow sweetRow = (SweetRow)item;
                    Category category = db.Category.Local.First(o => o.name.Equals(sweetRow.category));
                    Sweet sweet = new Sweet { id = sweetRow.id, Category = category, name = sweetRow.name, price_per_unit = sweetRow.price };
                    db.Sweet.AddOrUpdate(sweet);
                    break;
                case "Production":
                    ProductionRow productionRow = (ProductionRow)item;
                    Sweet productedSweet = db.Sweet.Local.First(o => o.name.Equals(productionRow.sweet));
                    ConfectionaryShop shop = db.ConfectionaryShop.First(o => o.name.Equals(productionRow.shop));
                    Production production = new Production
                    {
                        id = productionRow.id,
                        amount = productionRow.amount,
                        ConfectionaryShop = shop,
                        production_date = productionRow.date,
                        Sweet = productedSweet
                    };
                    db.Production.AddOrUpdate(production);
                    break;
            }
        }

        public object GetNewItem(string entityName)
        {
            switch (entityName)
            {
                case "Sweet":
                    int s_id = 1;
                    try
                    {
                        s_id = db.Sweet.Max(o => o.id) + 1;
                    }
                    catch (InvalidOperationException e) { }

                    SweetRow sweet = new SweetRow { id = s_id, name = "Placeholder", category = "Placeholder", price = 0 };
                    return sweet;
                case "Production":
                    int p_id = 1;
                    try
                    {
                        p_id = db.Production.Max(o => o.id) + 1;
                    }
                    catch (InvalidOperationException e) { }
                    ProductionRow production = new ProductionRow { id = p_id, sweet = "Placeholder", amount = 0, shop = "Placeholder", date = DateTime.Now };
                    return production;
            }

            return -1;
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }
    }
}
