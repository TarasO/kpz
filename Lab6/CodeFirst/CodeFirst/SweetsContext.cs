﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst
{
    class SweetsContext : DbContext
    {
        public SweetsContext() : base("DefaultConnection") { }

        public DbSet<Category> Category { get; set; }
        public DbSet<Sweet> Sweet { get; set; }
        public DbSet<ConfectionaryShop> ConfectionaryShop { get; set; }
        public DbSet<Production> Production { get; set; }
    }
}
