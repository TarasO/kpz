﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirst
{
    class Production
    {
        public int id { get; set; }
        //public int sweet_id { get; set; }
        public decimal amount { get; set; }
        //public int confectionary_shop_id { get; set; }
        public System.DateTime production_date { get; set; }

        public virtual ConfectionaryShop ConfectionaryShop { get; set; }
        public virtual Sweet Sweet { get; set; }
    }
}
