﻿namespace CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryDescriptionColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Categories", "description");
        }
    }
}
