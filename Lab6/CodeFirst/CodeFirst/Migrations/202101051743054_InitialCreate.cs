﻿namespace CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        unit = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Sweets",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        price_per_unit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Category_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Categories", t => t.Category_id)
                .Index(t => t.Category_id);
            
            CreateTable(
                "dbo.Productions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        production_date = c.DateTime(nullable: false),
                        ConfectionaryShop_id = c.Int(),
                        Sweet_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ConfectionaryShops", t => t.ConfectionaryShop_id)
                .ForeignKey("dbo.Sweets", t => t.Sweet_id)
                .Index(t => t.ConfectionaryShop_id)
                .Index(t => t.Sweet_id);
            
            CreateTable(
                "dbo.ConfectionaryShops",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productions", "Sweet_id", "dbo.Sweets");
            DropForeignKey("dbo.Productions", "ConfectionaryShop_id", "dbo.ConfectionaryShops");
            DropForeignKey("dbo.Sweets", "Category_id", "dbo.Categories");
            DropIndex("dbo.Productions", new[] { "Sweet_id" });
            DropIndex("dbo.Productions", new[] { "ConfectionaryShop_id" });
            DropIndex("dbo.Sweets", new[] { "Category_id" });
            DropTable("dbo.ConfectionaryShops");
            DropTable("dbo.Productions");
            DropTable("dbo.Sweets");
            DropTable("dbo.Categories");
        }
    }
}
