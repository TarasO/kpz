﻿using Microsoft.EntityFrameworkCore;

namespace Oliinyk.API.Models
{
    public class SweetsContext : DbContext
    {
        public SweetsContext(DbContextOptions<SweetsContext> options) : base(options) { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Sweet> Sweets { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Production> Productions { get; set; }
    }
}
