﻿namespace Oliinyk.API.Models
{
    public class Shop
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
