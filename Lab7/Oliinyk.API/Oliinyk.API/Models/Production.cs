﻿using System;

namespace Oliinyk.API.Models
{
    public class Production
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public int SweetId { get; set; }
        public int ShopId { get; set; }

        public virtual Sweet Sweet { get; set; }
        public virtual Shop Shop { get; set; }
    }
}
