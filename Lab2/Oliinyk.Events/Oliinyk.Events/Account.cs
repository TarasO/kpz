﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.Events
{

    class GameEventArgs : EventArgs
    {
        public GameEventArgs(string line)
        {
            this.line = line;
        }

        public string line { get; }
    }

    class WinEventArgs : EventArgs
    {
        public WinEventArgs(int guessed, int hidden)
        {
            this.guessed = guessed;
            this.hidden = hidden;
        }

        public int guessed { get; }
        public int hidden { get; }
    }

    class NumberGame
    {

        public event EventHandler<GameEventArgs> OnLess;
        public event EventHandler<GameEventArgs> OnMore;
        public event EventHandler<WinEventArgs> OnWin;

        public int number { get; private set; }

        public NumberGame()
        {
            Generate();
        }

        public void Generate()
        {
            Random random = new Random();
            number = random.Next(1, 1000);
        }

        public int Check(int num)
        {
            if (num < number)
            {
                OnLess?.Invoke(this, new GameEventArgs(num + " is less."));
            }

            if (num > number)
            {
                OnMore?.Invoke(this, new GameEventArgs(num + " is greater."));
            }

            if (num == number)
            {
                OnWin?.Invoke(this, new WinEventArgs(num, number));
            }

            return num.CompareTo(number);
        }
    }
}
