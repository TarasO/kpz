﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.Events
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberGame numberGame = new NumberGame();
            numberGame.OnLess += DisplayMessage;
            numberGame.OnMore += DisplayMessage;
            numberGame.OnWin += WinMessage;

            Console.WriteLine("Welcome to Number Guessing Game!");

            int input = 0;
            do
            {
                Console.Write("Input number: ");
                input = Convert.ToInt32(Console.ReadLine());
            } while (numberGame.Check(input) != 0);

            Console.ReadKey();
        }

        private static void DisplayMessage(object sender, GameEventArgs e)
        {
            Console.WriteLine(e.line);
            Console.WriteLine();
        }

        private static void WinMessage(object sender, WinEventArgs e)
        {
            Console.WriteLine("Congratulations! Your number " + e.guessed + " is the hidden number " + e.hidden);
        }
    }
}
