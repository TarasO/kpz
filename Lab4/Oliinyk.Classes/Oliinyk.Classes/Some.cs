﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.Classes
{
    class SomeClass
    {
        int A { get; set; }
        SomeInnerClass inner;

        public SomeClass()
        {
            A = 0;
            inner = new SomeInnerClass();
        }

        class SomeInnerClass
        {
            public int B { get; set; }
        }
    }

    struct SomeStruct
    {
        public int a;
        int b;
    }

    public class SomePublicClass
    {
        NotPublicInnerClass innerClass { get; set; }

        public SomePublicClass()
        {
            innerClass = new NotPublicInnerClass();
        }

        private class NotPublicInnerClass
        {
            public int C { get; set; }
        }
    }

    public interface InterfaceA
    { 
        void showA();
    }

    public class ClassA : InterfaceA
    {
        public void showA()
        {
            Console.WriteLine("A");
        }
    }

    public interface InterfaceB
    {
        void showB();
    }

    public class ClassB : InterfaceB
    {
        public void showB()
        {
            Console.WriteLine("B");
        }
    }

    public class SomeClassMultiple : InterfaceA, InterfaceB
    {
        private ClassA a;
        private ClassB b;

        public SomeClassMultiple()
        {
            a = new ClassA();
            b = new ClassB();
        }

        public void showA()
        {
            a.showA();
        }

        public void showB()
        {
            b.showB();
        }
    }

    public class FieldTypeClass
    {
        public static int a = 42;
        
        private static int b;
        private int C { get; set; } = 42;

        static FieldTypeClass()
        {
            b = 42;
        }

        public FieldTypeClass(int c)
        {
            this.C = c;
        }
    }

}
