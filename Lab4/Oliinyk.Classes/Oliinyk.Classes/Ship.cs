﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.Classes
{
    public abstract class Ship : Moveable
    {
        protected Ship()
        {
        }

        protected Ship(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public void move(int dX, int dY)
        {
            X += dX;
            Y += dY;
        }

        protected int X { get; set; }
        protected int Y { get; set; }
    }
}
