﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.Classes
{
    interface Moveable
    {
        void move(int dX, int dY);
    }
}
