﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oliinyk.Classes;

namespace Oliinyk.Project
{
    public class Player : Ship
    {
        private string name {get;}
        private int damage;
        public Player(int x, int y, string name) : base(x, y)
        {
            this.name = name;
        }

        public Player(int x, int y, string name, int damage) : this(x, y, name)
        {
            this.damage = damage;
        }

        public void Shoot(Enemy enemy)
        {
            enemy.Health -= damage;
        }

        public void Shoot(Enemy enemy, double modifier)
        {
            enemy.Health -= (int)(damage * modifier);
        }
    }
}
