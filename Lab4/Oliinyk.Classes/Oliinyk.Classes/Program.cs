﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oliinyk.Project;

namespace Oliinyk.Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            //Enemy enemy = new Enemy(100, EnemyType.GREENBAT);
            //enemy.Health = 50;
            //enemy.EnemyType = EnemyType.REDHAWK;
            //enemy.

            //SomeClass someClass = new SomeClass();

            //SomeStruct someStruct = new SomeStruct();
            //someStruct.a = 10;

            //SomePublicClass somePublicClass = new SomePublicClass();
            //somePublicClass.

            //Console.WriteLine(EnemyType.GREENBAT ^ EnemyType.YELLOWOWL);
            //Console.WriteLine(EnemyType.GREENBAT || EnemyType.YELLOWOWL);
            //Console.WriteLine(EnemyType.GREENBAT && EnemyType.YELLOWOWL);
            //Console.WriteLine(EnemyType.GREENBAT & EnemyType.YELLOWOWL);
            //Console.WriteLine(EnemyType.GREENBAT | EnemyType.YELLOWOWL);

            //SomeClassMultiple someClassMultiple = new SomeClassMultiple();
            //someClassMultiple.showA();
            //someClassMultiple.showB();

            //int a = 5;
            //int b;
            //Sqr(a);
            //Console.WriteLine("a: " + a);
            //Sqr(ref a);
            //Console.WriteLine("a: " + a);
            //Sqr(a, out b);
            //Console.WriteLine("a: " + a + ", b: " + b);

            //int a = 42;
            //object box = (object)42;
            //int unbox = (int)box;

            //Console.WriteLine(a);
            //Console.WriteLine(box);
            //Console.WriteLine(unbox);

            int a = 42;
            long b = a;
            long c = (long)a;
            int d = Int32.Parse("42");

            TwoIntTwoFieldClass[] arr = new TwoIntTwoFieldClass[30000000];
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = new TwoIntTwoFieldClass(i, i);
            }
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);
            Console.Read();
        }

        public static void Sqr(int a)
        {
            a = a * a;
        }

        public static void Sqr(ref int a)
        {
            a = a * a;
        }
        public static void Sqr(int a, out int b)
        {
            b = a * a;
        }
    }
}
