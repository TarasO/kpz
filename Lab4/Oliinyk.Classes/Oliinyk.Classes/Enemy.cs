﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oliinyk.Classes;

namespace Oliinyk.Project
{

    public enum EnemyType
    {
        GREENBAT = 0, YELLOWOWL = 1, REDHAWK = 2
    }

    public class Enemy : Ship
    {
        public Enemy(int health, EnemyType enemyType)
        {
            Random random = new Random();
            base.X = random.Next(0, 600);
            base.Y = 0;
            this.Health = health;
            this.EnemyType = enemyType;
        }

        public int Health { get; set; }
        public EnemyType EnemyType { get; private set; }

        public static implicit operator Enemy(int health)
        {
            return new Enemy(health, (EnemyType)(health % 3));
        }

        public static explicit operator int(Enemy enemy)
        {
            return enemy.Health;
        }

    }

    public class EnemyGenericComparator : IComparer<Enemy>
    {
        public int Compare(Enemy x, Enemy y)
        {
            return x.Health.CompareTo(y.Health);
        }
    }
    public class EnemyComparator : IComparer
    {
        public int Compare(object x, object y)
        {
            Enemy xEnemy = (Enemy)x;
            Enemy yEnemy = (Enemy)y;
            return xEnemy.Health.CompareTo(yEnemy.Health);
        }
    }
}
