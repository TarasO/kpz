﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oliinyk.Classes
{
    public class EmptyClass
    {
        public EmptyClass()
        {
        }
    }
    public interface INoField1
    {
        
    }

    public interface INoField2
    {
        
    }

    public interface IOneField1
    {
        int a
        {
            get;
            set;
        }
    }

    public interface IOneField2
    {
        int b
        {
            get;
            set;
        }
    }

    public class OneIntNoFieldClass : INoField1
    {
        public OneIntNoFieldClass()
        {
        }
    }

    public class TwoIntNoFieldClass : INoField1, INoField2
    {
        public TwoIntNoFieldClass()
        {
        }
    }

    public class OneIntOneFieldClass : IOneField1
    {
        public OneIntOneFieldClass(int a)
        {
            this.a = a;
        }

        public int a { get; set; }
    }

    public class TwoIntOneFieldClass : IOneField1, INoField1
    {
        public TwoIntOneFieldClass(int a)
        {
            this.a = a;
        }

        public int a { get; set; }
    }

    public class TwoIntTwoFieldClass : IOneField1, IOneField2
    {
        public TwoIntTwoFieldClass(int a, int b)
        {
            this.a = a;
            this.b = b;
        }

        public int a { get; set; }
        public int b { get; set; }
    }
}
